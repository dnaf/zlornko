# Zlornko Teaches Typing
My point-and-click typing tutor game for Ludum Dare 41

See the Ludum Dare page [here](https://ldjam.com/events/ludum-dare/41/zlornko-teaches-typing)

![Gameplay](./branding/gameplaylong.gif)
