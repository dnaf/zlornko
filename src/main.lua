-- Hey! This code is awful because I completely overestimated how long 48 hours is
-- If you're a future employer, please don't scroll down!

local keys = {
	{"q", "w", "e", "r", "t", "y", "u", "i", "o", "p"},
	{"a", "s", "d", "f", "g", "h", "j", "k", "l"},
	{"z", "x", "c", "v", "b", "n", "m"}
}

local failKeys = {
	{nil, {4, 1}, nil, {1, 0}, {2, 0}, {5, 0}, nil, {8, 1}, {8, 2}},
	{{5, 1}, nil, {6, 2}, {1, 1}, {0, 0}, nil, {8, 0}, {6, 1}, {10, 1}},
	{{0, 2}, {4, 2}, {0, 1}, {10, 0}, {10, 2}, {11, 2}, {12, 2}}
}

local lastHealth = 0
local dictionary = {
	"klam",
	"flam",
	"dert",
	"flang",
	"florng",
	"donge",
	"dongus",
	"flongus",
	"wumpus",
	"dumbus",
	"dimbus",
	"schlang",
	"prongus",
	"flarmgop",
	"flarm",
	"slarm",
	"wimblop",
	"scrimble",
	"scrimblongus",
	"adangatang",
	"heckohjeebz",
	"frangepopper",
	"dongoblimpus",
	"florange",
	"shlingoboopbop",
	"brongopogopogop",
	"shlangus",
	"schlorngop",
	"schlorngopulous",
	"dlangorpalous"
}

local dialogue = {
	start = {
		"LETS DO THIS",
		"HERE WE GO",
		"I KNOW YOU CAN DO IT",
		"GOOD LUCK"
	},
	hundo = {
		"YOURE DOING GREAT",
		"FANTASTIC",
		"AMAZING",
		"KEEP IT UP"
	},
	dying = {
		"OH NO",
		"OH JEEZ",
		"OH HECK",
		"HECK",
		"HOO"
	},
	dead = {
		"I KNOW YOU CAN DO BETTER NEXT TIME",
		"DO YOU WANT TO TRY AGAIN",
		"THAT WASNT THAT BAD",
		"THAT WAS A TOUGH ONE. YOULL GET IT NEXT TIME"
	}
}
local currentDialogue = ""
local dialogueProgress = 0
local dialogueLetterCooldown = 0

local leaderboard = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

local health = 3
local score = 0

local expectedWord = ""
local wordProgress = 0
local history = {}

local gameCanvas
local keySize = 24

local keyboardOffset = {32, 200}
local waveIntensity = 32
local waveSpeed = 5
local waveLength = 3

local hWaveIntensity = 32
local hWaveSpeed = 2
local hWaveLength = 0.01

local zlornkoState = nil
local zlornkos = {}
local font
local letterParticles

local lastWordTime
local inGame = false
local inMenu = true
local dyingTimer = 0

local difficulty = 0

local sources = {
	key = {},
	keyi = 1,
	bad = {},
	badi = 1,
	fail = {},
	faili = 1,
	speak = {},
	speaki = 1
}

local bgm = {
	bgm = {}
}

local tryAgain = {
	bx = 240 - 9 * 4 - 10,
	by = 310,
	bw = 9 * 8 + 20,
	bh = 28,
	x = 240 - 9 * 4,
	y = 320,
}

function nextWord()
	lastWordTime = love.timer.getTime()
	if difficulty < 10 then
		expectedWord = dictionary[math.floor(love.math.random() * #dictionary) + 1]
	else
		expectedWord = "obey"
	end
	wordProgress = 0

	newAnimation()
end

function newAnimation()
	local dir = love.math.random() * 6.28
	local x = math.sin(dir)
	local y = math.cos(dir)
	waveSpeed = y + love.math.random() * 10 * y
	hWaveSpeed = x + love.math.random() * 10 * x
	waveIntensity = math.min(math.min(love.math.random() * 64 * y * difficulty + difficulty * 10, 90 /  math.abs(waveSpeed)), 150)
	hWaveIntensity = math.min(math.min(love.math.random() * 64 * x * difficulty + difficulty * 10, 100 / math.abs(hWaveSpeed)), 150)

	local i = love.math.random() * 6
	waveLength = i * y / 3
	hWaveLength = i * y / 60
end


function love.load()
	gameCanvas = love.graphics.newCanvas(480, 360)
	gameCanvas:setFilter("nearest")

	-- Load leaderboard
	love.filesystem.append("scores", "") -- Dirty way to ensure file exists
	for line in love.filesystem.lines("scores") do
		if tonumber(line) ~= nil then
			table.insert(leaderboard, tonumber(line))
		end
	end
	table.sort(leaderboard)

	
	-- Load assets
	for _, file in pairs(love.filesystem.getDirectoryItems("zlornkos")) do
		local name = file:gsub(".png$", "")
		zlornkos[name] = love.graphics.newImage("zlornkos/" .. file)
		zlornkos[name]:setFilter("nearest")
	end
	local fontImageData = love.image.newImageData("font.png")
	local fontImage = love.graphics.newImage(fontImageData)
	font = love.graphics.newImageFont(fontImageData, " ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890.$")

	letterParticles = love.graphics.newParticleSystem(fontImage, 1000)

	local fontquads = {}

	for i = 1, 26 do
		fontquads[i] = love.graphics.newQuad(i * 9 + 1, 0, 8, 8, fontImage:getDimensions())
	end

	letterParticles:setQuads(fontquads)
	letterParticles:setParticleLifetime(1, 10)
	letterParticles:setEmissionRate(100)
	letterParticles:setSizeVariation(1)
	letterParticles:setEmissionArea("uniform", 300, 300)


	-- Sounds
	for i = 1,10 do
		sources.key[i] = love.audio.newSource("sounds/key.ogg", "static")
		sources.bad[i] = love.audio.newSource("sounds/bad.ogg", "static")
	end
	sources.fail[1] = love.audio.newSource("sounds/fail.ogg", "static")

	for i = 1,10 do
		sources.speak[i] = love.audio.newSource("sounds/speak.ogg", "static")
		sources.speak[i]:setVolume(0.5)
	end

	-- Music
	bgm.bgm.intro = love.audio.newSource("music/bgm_intro.ogg", "stream")
	bgm.bgm.loop = love.audio.newSource("music/bgm_loop.ogg", "stream")
	bgm.bgm.loop:setLooping(true)

	bgm.sad = love.audio.newSource("music/sad.ogg", "stream")
end

function sayDialogue(form)
	if dialogue[form] then
		form = dialogue[form][math.floor(love.math.random() * #dialogue[form]) + 1]
	end

	currentDialogue = form
	dialogueProgress = 0
end

function startNewGame()
	sayDialogue("start")
	history = {}
	tryAgainButton = false
	letterParticles:start()
	letterParticles:setLinearAcceleration(0, 0)
	letterParticles:setSpeed(10, 150)
	difficulty = 0
	score = 0
	bgm.bgm.loop:stop()
	bgm.sad:stop()
	inMenu = false
	dying = false
	dyingTimer = 0
	inGame = true
	health = 3
	nextWord()
	bgm.bgm.intro:play()
	bgm.bgm.cur = bgm.bgm.intro
end

function getKeyPos(x, y)
	local playX, playY = x * (keySize + 6) + math.cos(love.timer.getTime() * hWaveSpeed + y / hWaveLength) * hWaveIntensity + keyboardOffset[1], (y * (keySize + 6)) + math.sin(love.timer.getTime() * waveSpeed + x / waveLength) * waveIntensity + keyboardOffset[2]

	local failX, failY = unpack((failKeys[y] or {})[x] or {12, 12})
	failX = failX * (keySize + 6) + keyboardOffset[1] + math.sin(love.timer.getTime() * 2.222) * 3
	failY = failY * (keySize + 6) + keyboardOffset[2] + math.cos(love.timer.getTime() + failX / 256) * 16

	local t = math.max(math.min(dyingTimer - 1 - x / 11, 1), 0)

	local rx = (failX - playX) * t + playX
	local ry = (failY - playY) * t + playY

	return rx, ry
end

function updateLeaderboard()
	table.insert(leaderboard, score)
	table.sort(leaderboard)
	while #leaderboard > 10 do
		table.remove(leaderboard, 1)
	end
	love.filesystem.write("scores", table.concat(leaderboard, "\n"))
end

function love.draw()
	love.graphics.push("all")
	love.graphics.setCanvas(gameCanvas)

	if inGame or dying then
		local beat = ((bgm.bgm.cur:tell() / 60) * 160) % 1
		beat = (1 - beat)
		beat = beat * beat * beat

		love.graphics.setColor(0, 0, 0, 0.1)
		love.graphics.rectangle("fill", 0, 0, 480, 360)

		if beat > 0 and bgm.bgm.cur:isPlaying() then
			local c = beat
			if bgm.bgm.cur == bgm.bgm.intro then
				c = (bgm.bgm.cur:tell() / 60) * 160 - 16
				c = c / (8 * 4)
				c = math.max(math.min(c, 1), 0)
				c = c * beat
			end
			local colors = {{1,0,0,c},{1,1,0,c},{0,1,0,c},{0,1,1,c},{0,0,1,c},{1,0,1,c}}
			local t = math.floor((bgm.bgm.cur:tell() / 60) * 160) % #colors
			love.graphics.setColor(colors[t + 1], beat * c)
			love.graphics.rectangle("fill", 0, 0, 480, 360)
		end

		love.graphics.setFont(font)

		love.graphics.setColor(1, 1, 1, 0.1)
		love.graphics.draw(letterParticles, 240, 180)

		-- Draw buffer
		love.graphics.setColor(1, 1, 1, 1)
		local buffer = expectedWord:sub(1, wordProgress)
		love.graphics.print(buffer:upper(), 40, 150)

		if not dying then
			-- Draw history
			love.graphics.setColor(1, 1, 1, 1)
			for i, v in pairs(history) do
				love.graphics.print(v[1]:upper() .. " $" .. math.floor(v[2] / 10) / 10 .. "0", 32, 130 - (i + 1) * 8)
			end
		else
			if dyingTimer > 2 then
				for i = 1,10 do
					local v = tostring(leaderboard[i] or 0)
					v = "ZLORNKO" .. string.rep(".", 10 - #v) .. v
					if leaderboard[i] == score and score ~= 0 then
						love.graphics.setColor(1, 1, 0, 1)
					else
						love.graphics.setColor(1, 1, 1, 1)
					end
					local scroll = -200 * (4 - math.max(math.min(dyingTimer, 4), 2)) / 2
					love.graphics.print(v, scroll + 48 + math.sin(dyingTimer + i / 4) * 3, 120 - (i * 8))
				end
			end
		end

		-- Draw expected word
		love.graphics.setColor(1, 1, 1, 1)
		if not dying or dyingTimer < 2.5 then
			love.graphics.print("PLEASE TYPE: " .. expectedWord:upper(), 32, 130)
		else
			love.graphics.print("YOU FLONKED: " .. expectedWord:upper(), 32, 130)
		end

		-- Draw zlornko
		love.graphics.draw(zlornkos[zlornkoState or "idle"] or zlornkos["idle"], 300, 32, 0, 2, 2)
		love.graphics.setColor(1, 0, 1, 1)
		love.graphics.print("ZLORNKO", 330, 140)

		-- Draw zlornkos magical words
		love.graphics.setColor(1, 1, 1, 1)
		love.graphics.printf(currentDialogue:sub(1, dialogueProgress), 300, 150, zlornkos[zlornkoState or "idle"]:getWidth() * 2)
		
		-- Draw health
		love.graphics.setColor(0, 0, 0, 1)
		love.graphics.rectangle("fill", 0, 0, 480, 10)

		love.graphics.setColor(1, 0, 0, 1)
		love.graphics.rectangle("fill", 0, 0, 480 * health / 3, 8)
		if health > 1 then
			love.graphics.setColor(0, 1, 0, 1)
			love.graphics.rectangle("fill", 480 / 3, 0, 480 * (health - 1) / 3, 8)
			if health > 2 then
				love.graphics.setColor(0, 0, 1, 1)
				love.graphics.rectangle("fill", (480 / 3) * 2, 0, 480 * (health - 2) / 3, 8)
			end
		end

		-- Draw score
		love.graphics.setColor(1, 1, 1, 1)
		love.graphics.print("SCORE: " .. score, 12, 12)

		-- Draw keys
		for row in ipairs(keys) do
			for col, k in ipairs(keys[row]) do
				local x, y = getKeyPos(col, row)
				love.graphics.setColor(0, 0, 0, 1)
				love.graphics.rectangle("fill", x, y, keySize, keySize)
				if currentKey == k and currentKeyState == "pressed" then
					rectType = "fill"
					textColor = {0, 0, 0, 1}
				else
					rectType = "line"
					textColor = {1, 1, 1, 1}
				end
				love.graphics.setColor(1, 1, 1, 1)
				love.graphics.rectangle(rectType, x, y, keySize, keySize)
				love.graphics.setColor(textColor)
				love.graphics.print(k:upper(), x + 8, y + 8)
			end
		end

		-- Draw obnoxious watermark
		love.graphics.setColor(0.2, 0.2, 0.2, 1)
		love.graphics.print("MADE IN 48 HOURS BY KATIE WOLFE FOR LUDUM DARE 41", 0, 352)

		-- Draw try again button
		if tryAgainButton then
			love.graphics.setColor(0, 0, 0, 1)
			love.graphics.rectangle("fill", tryAgain.bx, tryAgain.by, tryAgain.bw, tryAgain.bh)
			love.graphics.setColor(1, 1, 1, 1)
			love.graphics.print("TRY AGAIN", tryAgain.x, tryAgain.y)
			love.graphics.rectangle("line", tryAgain.bx, tryAgain.by, tryAgain.bw, tryAgain.bh)
		end
	elseif inMenu then
		love.graphics.clear()
		love.graphics.setFont(font)
		for i = 1, 24 do
			love.graphics.print("CLICK TO START LEARNING WITH ZLORNKO", 64, 64 + i * 8)
		end
	end

	love.graphics.pop()
	love.graphics.draw(gameCanvas, 0, 0, 0, 2, 2)
end

function love.update(dt)
	letterParticles:update(dt)

	health = health - dt / 2
	if health <= 0 and inGame then
		updateLeaderboard()
		currentDialogue = ""
		inGame = false
		dying = true
		bgm.bgm.cur:stop()
		letterParticles:setLinearAcceleration(0, 100)
		letterParticles:setSpeed(0, 0)
		playSound("fail")
	elseif health < math.floor(lastHealth) and inGame then
		sayDialogue("dying")
	end

	if bgm.bgm.intro:isPlaying() and bgm.bgm.intro:tell() >= 16 then
		bgm.bgm.loop:seek(bgm.bgm.intro:tell() - 12)
		bgm.bgm.intro:stop()
		bgm.bgm.loop:play()
		bgm.bgm.cur = bgm.bgm.loop
	end

	lastHealth = health

	if dying then
		dyingTimer = dyingTimer + dt
		if dyingTimer > 2.5 and not tryAgainButton then
			bgm.sad:play()
			tryAgainButton = true
		end
		if dyingTimer > 3 and #currentDialogue == 0 then
			sayDialogue("dead")
		end
	end

	dialogueLetterCooldown = dialogueLetterCooldown + dt
	if dialogueProgress < #currentDialogue and dialogueLetterCooldown > 0.1 then
		dialogueLetterCooldown = 0
		dialogueProgress = dialogueProgress + 1
		if currentDialogue:sub(dialogueProgress, dialogueProgress) ~= " " then
			playSound("speak")
		end
	end
end

-- whoooo bad code!
function getKeyAt(x, y)
	local lastKey = nil
	for row in ipairs(keys) do
		for col, k in ipairs(keys[row]) do
			local kx, ky = getKeyPos(col, row)
			if x >= kx and y >= ky and x < kx + keySize and y < ky + keySize then
				lastKey = k
			end
		end
	end
	return lastKey
end

function playSound(name)
	sources[name][sources[name .. "i"]]:stop()
	sources[name][sources[name .. "i"]]:play()
	sources[name .. "i"] = (sources[name .. "i"] % #sources[name]) + 1
end

function love.mousepressed(x, y, b)
	x = x / 2
	y = y / 2
	
	if inGame then
		if b == 1 then
			local k = getKeyAt(x, y)
			if k ~= nil then
				currentKey = k
				currentKeyState = "pressed"
				if k == expectedWord:sub(wordProgress + 1, wordProgress + 1) then
					playSound("key")
					wordProgress = wordProgress + 1
					difficulty = difficulty + 0.05
					if wordProgress == #expectedWord then
						local timeSinceLastWord = love.timer.getTime() - lastWordTime
						local prevScore = score
						score = math.floor(score + #expectedWord * 100 / timeSinceLastWord)
						if prevScore % 500 > score % 500 then
							sayDialogue("hundo")
						end
						table.insert(history, 1, {expectedWord, score - prevScore})
						nextWord()
					end
					health = math.ceil(health)
				else
					playSound("bad")
					health = math.floor(health)
					if health > 0 then
						newAnimation()
					end
				end
				--buffer = buffer .. k
			end
		end
	elseif inMenu then
		startNewGame()
	elseif dying then
		if x >= tryAgain.bx and y >= tryAgain.by and x <= tryAgain.bx + tryAgain.bw and y <= tryAgain.by + tryAgain.bh then
			startNewGame()
		end
	end
end

function love.mousereleased(x, y, b)
	currentKeyState = "off"
end
